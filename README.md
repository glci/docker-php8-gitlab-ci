# PHP 8.0 Docker image for GitLab CI

An optimized docker image for PHP 8.0 projects (mostly Laravel) on GitLab CI for debugging, unit testing, etc.

## Usage

As the Docker image is automatically build on Docker Hub, you can use it right in your pipeline:

```yaml
image: 'registry.gitlab.com/glci/docker-php-gitlab-ci'

cache:
  paths:
    - vendor/
    - node_modules/

before_script:
  - composer install

...
```

## Development

To deploy on the container registry on GitLab: 
```shell
docker login registry.gitlab.com

docker build -t registry.gitlab.com/glci/docker-php8-gitlab-ci .
docker push registry.gitlab.com/glci/docker-php8-gitlab-ci
```
